package smallbrother;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author  dv6
 */
public class Meeting
{

	/**
	 * @uml.property  name="id"
	 */
	private long id = 0;
	/**
	 * @uml.property  name="begin"
	 */
	private Date begin;
	/**
	 * @uml.property  name="end"
	 */
	private Date end;

	public Meeting(Date begin, Date end)
	{
		this.begin = begin;
		this.end = end;
	}

	/**
	 * @return
	 * @uml.property  name="begin"
	 */
	public Date getBegin()
	{
		return begin;
	}

	/**
	 * @return
	 * @uml.property  name="end"
	 */
	public Date getEnd()
	{
		return end;
	}

	/**
	 * @return
	 * @uml.property  name="id"
	 */
	public long getId()
	{
		return id;
	}

	/**
	 * @param id
	 * @uml.property  name="id"
	 */
	public void setId(long id)
	{
		this.id = id;
	}

	@Override
	public String toString()
	{
		final StringBuilder sb = new StringBuilder();
		sb.append("Meeting");
		sb.append("{id=").append(id);
		sb.append(", begin=").append(begin);
		sb.append(", end=").append(end);
		sb.append('}');
		return sb.toString();
	}

	public String toXML()
	{
		// Here we should use the Decorator Pattern
		// but maybe later :)
		final StringBuilder sb = new StringBuilder();
		sb.append("<Meeting>\n");
		sb.append("<Id>").append(id).append("</Id>\n");
		// sb.append("<Begin>").append(begin).append("</Begin>\n");
		sb.append("<Begin>")
				.append(DateFormat.getDateTimeInstance().format(begin))
				.append("</Begin>\n");
		// sb.append("<End>").append(end).append("</End>\n");
		sb.append("<End>").append(DateFormat.getDateTimeInstance().format(end))
				.append("</End>\n");
		sb.append("</Meeting>\n");
		return sb.toString();
	}
}
