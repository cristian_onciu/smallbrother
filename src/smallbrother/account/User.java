package smallbrother.account;

import smallbrother.Meeting;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * @author  dv6
 */
public class User extends Account implements Serializable
{

	/**
	 * @uml.property  name="sick"
	 */
	private boolean sick;
	/**
	 * @uml.property  name="insideBuilding"
	 */
	private boolean insideBuilding;
	/**
	 * @uml.property  name="meetings"
	 */
	private List<Meeting> meetings;

	public User(String name, String password)
	{
		super(AccountType.USER, name, password);
		this.sick = false;
		this.insideBuilding = false;
		this.meetings = new ArrayList<Meeting>();
	}

	/**
	 * @param insideBuilding
	 * @uml.property  name="insideBuilding"
	 */
	public void setInsideBuilding(boolean insideBuilding)
	{
		this.insideBuilding = insideBuilding;
	}


	/**
	 * @return
	 * @uml.property  name="insideBuilding"
	 */
	public boolean isInsideBuilding()
	{
		return insideBuilding;
	}


	/**
	 * @param sick
	 * @uml.property  name="sick"
	 */
	public void setSick(boolean sick)
	{
		this.sick = sick;
	}


	/**
	 * @return
	 * @uml.property  name="sick"
	 */
	public boolean isSick()
	{
		return sick;
	}

	public void scheduleMeeting(Meeting meeting)
	{
		meetings.add(meeting);
	}

	public void removeMeeting(long meetingID)
	{
		List<Meeting> meetings2 = new CopyOnWriteArrayList<Meeting>();
		meetings2.addAll(meetings);
		for (Meeting meeting : meetings2)
			if (meeting.getId() == meetingID)
				meetings2.remove(meeting);
		meetings = meetings2;
	}


	/**
	 * @return
	 * @uml.property  name="meetings"
	 */
	public List<Meeting> getMeetings()
	{
		return meetings;
	}

	@Override
	public String toString()
	{
		final StringBuilder sb = new StringBuilder();
		sb.append("User");
		sb.append("{name='").append(this.getName()).append('\'');
		;
		sb.append(", password='").append(this.getPassword()).append('\'');
		;
		sb.append(", sick=").append(sick);
		sb.append(", insideBuilding=").append(insideBuilding);
		sb.append(", meetings=").append(meetings);
		sb.append('}');
		return sb.toString();
	}

	@Override
	public boolean equals(Object o)
	{
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;

		User user = (User) o;

		if (insideBuilding != user.insideBuilding)
			return false;
		if (sick != user.sick)
			return false;
		if (meetings != null ? !meetings.equals(user.meetings)
				: user.meetings != null)
			return false;
		if (!this.getName().equals(user.getName()))
			return false;
		if (!this.getPassword().equals(user.getPassword()))
			return false;

		return true;
	}

	@Override
	public int hashCode()
	{
		int result = this.getName().hashCode();
		result = 31 * result + this.getPassword().hashCode();
		result = 31 * result + (sick ? 1 : 0);
		result = 31 * result + (insideBuilding ? 1 : 0);
		result = 31 * result + (meetings != null ? meetings.hashCode() : 0);
		return result;
	}
}
