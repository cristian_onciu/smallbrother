package smallbrother.account;

import smallbrother.Meeting;
import smallbrother.xml.XMLFactory;

public class Admin extends Account
{
    public Admin(String name, String password) {
        super(AccountType.ADMIN, name, password);
    }

    public void registerSickEmployee(User user)
    {
        user.setSick(true);
    }

    public void scheduleMeeting(User user,Meeting meeting)
    {
        user.scheduleMeeting(meeting);
    }

    public void registerEmployee(User user)
    {
        user.setInsideBuilding(true);
    }

    public void unregisterEmployee(User user)
    {
        user.setInsideBuilding(false);
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer();
        sb.append("Admin");
        sb.append("{name='").append(this.getName()).append('\'');
        sb.append(", password='").append(this.getPassword()).append('\'');
        sb.append('}');
        return sb.toString();
    }

    public String toXML()
    {
        return XMLFactory.toXML(this);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Admin admin = (Admin) o;

        if (!this.getName().equals(admin.getName())) return false;
        if (!this.getPassword().equals(admin.getPassword())) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = this.getName().hashCode();
        result = 31 * result + this.getPassword().hashCode();
        return result;
    }
}
