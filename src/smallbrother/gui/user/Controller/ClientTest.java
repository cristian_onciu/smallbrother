package smallbrother.gui.user.Controller;

import smallbrother.gui.user.Controller.UserController;
import smallbrother.gui.user.View.RegularUserView;

import java.rmi.RemoteException;

import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;


public class ClientTest
{
    public static void main(String[] args)
    {
    	
    	try
		{
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		}
		catch (UnsupportedLookAndFeelException e)
		{
			System.out.println("Unsupported Look");
		}
		catch (ClassNotFoundException e)
		{
			System.out.println("Class not found");
		}
		catch (InstantiationException e)
		{
			System.out.println("InstantiationException");
		}
		catch (IllegalAccessException e)
		{
			System.out.println("Illegal Access");
		}
        UserController controller;
        try {
            controller=new UserController();
            RegularUserView view =new RegularUserView(controller);
            view.setVisible(true);
        } catch (RemoteException e) {
            e.printStackTrace(); 
        }
    }
}
