package smallbrother.gui.admin.View;

import java.awt.BorderLayout;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import javax.swing.*;
import javax.swing.border.EmptyBorder;
import smallbrother.gui.admin.Controller.AdminController;

/**
 * @author  dv6
 */
public class AdminView extends JFrame
{
	Image icon = Toolkit.getDefaultToolkit().getImage("adminIcon.gif");
	private JPanel contentPane;
	private DefaultListModel allEmpModel = new DefaultListModel();
	private DefaultListModel sickEmpModel = new DefaultListModel();
	private DefaultListModel loggedInEmpModel = new DefaultListModel();
	private DefaultListModel loggedOutEmpModel = new DefaultListModel();
	private JList listOfAllEmps = new JList(allEmpModel);
	private JList listOfSickEmps = new JList(sickEmpModel);
	private JList listOfLiEmps = new JList(loggedInEmpModel);
	private JList listOfLoEmps = new JList(loggedOutEmpModel);
	private JSeparator separator = new JSeparator();
	private JComboBox displayComboBox = new JComboBox();
	private String[] displayOptions = new String[]
	{ "Show all employees", "Show sick employees", "Show employees logged in",
			"Show employees logged out" };
	private JPanel listPanel = new JPanel();
	private JPanel mainPanel = new JPanel();
	private JMenuBar menuBar = new JMenuBar();
	private JMenu fileMenu = new JMenu("File");
	private JMenuItem mItemSaveFile = new JMenuItem("Save");
	private JMenuItem mItemExitProgram = new JMenuItem("Exit Program");
	private JMenu employeeMenu = new JMenu("Employee");
	private JMenuItem mItemCreateEmployee = new JMenuItem("Create");
	private JMenuItem mItemRemoveEmployee = new JMenuItem("Remove");
	private JMenuItem mItemRegisterSick = new JMenuItem("Register Sick");
	private JMenuItem mItemRegisterHealthy = new JMenuItem("Register Healthy");
	private JMenuItem mItemRegisterLoggedIn = new JMenuItem(
			"Register Logged in");
	private JMenuItem mItemRegisterLoggedOut = new JMenuItem(
			"Register Logged out");
	private JMenu meetingsMenu = new JMenu("Meetings");
	private JMenuItem mItemViewMeetings = new JMenuItem("View Meetings");
	private JMenuItem mItemScheduleMeetings = new JMenuItem("Schedule Meetings");
	
	/**
	 * @uml.property  name="controller"
	 * @uml.associationEnd  
	 */
	private AdminController controller;

	public AdminView(AdminController controller)
	{
		this.controller = controller;
		setTitle("Small Brother - Admin");
		setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		setBounds(100, 100, 275, 390);
		setResizable(false);
		setIconImage(icon);

		setJMenuBar(awesomeMenu());

		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);

		contentPane.add(mainPanel, BorderLayout.CENTER);
		mainPanel.setLayout(null);
		listOfAllEmps.setBounds(10, 11, 237, 256);
		listOfSickEmps.setBounds(10, 11, 237, 256);
		listOfLiEmps.setBounds(10, 11, 237, 256);
		listOfLoEmps.setBounds(10, 11, 237, 256);
		listPanel.setBounds(10, 11, 237, 256);
		separator.setBounds(10, 278, 237, 2);

		mainPanel.add(listOfAllEmps);
		mainPanel.add(listOfSickEmps);
		mainPanel.add(listOfLiEmps);
		mainPanel.add(listOfLoEmps);
		mainPanel.add(separator);

		displayComboBox.setModel(new DefaultComboBoxModel(displayOptions));
		displayComboBox.setBounds(20, 291, 215, 20);
		mainPanel.add(displayComboBox);

		displayComboBox.addActionListener(new DisplayBoxHandler());
		openXML();
	}

	public JMenuBar awesomeMenu()
	{
		menuBar.add(fileMenu);
		fileMenu.add(mItemSaveFile);
		fileMenu.add(mItemExitProgram);
		menuBar.add(employeeMenu);
		menuBar.add(meetingsMenu);
		employeeMenu.add(mItemCreateEmployee);
		employeeMenu.add(mItemRemoveEmployee);
		employeeMenu.add(mItemRegisterSick);
		employeeMenu.add(mItemRegisterHealthy);
		employeeMenu.add(mItemRegisterLoggedIn);
		employeeMenu.add(mItemRegisterLoggedOut);
		meetingsMenu.add(mItemViewMeetings);
		meetingsMenu.add(mItemScheduleMeetings);
		mItemSaveFile.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S,
				InputEvent.CTRL_MASK | InputEvent.ALT_MASK));
		mItemExitProgram.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_Q,
				InputEvent.ALT_MASK));
		mItemCreateEmployee.setAccelerator(KeyStroke.getKeyStroke(
				KeyEvent.VK_C, InputEvent.ALT_MASK));
		mItemRemoveEmployee.setAccelerator(KeyStroke.getKeyStroke(
				KeyEvent.VK_R, InputEvent.ALT_MASK));
		mItemRegisterSick.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S,
				InputEvent.ALT_MASK));
		mItemRegisterHealthy.setAccelerator(KeyStroke.getKeyStroke(
				KeyEvent.VK_H, InputEvent.ALT_MASK));
		mItemRegisterLoggedIn.setAccelerator(KeyStroke.getKeyStroke(
				KeyEvent.VK_I, InputEvent.ALT_MASK));
		mItemRegisterLoggedOut.setAccelerator(KeyStroke.getKeyStroke(
				KeyEvent.VK_U, InputEvent.ALT_MASK));
		mItemViewMeetings.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_V,
				InputEvent.ALT_MASK));
		mItemScheduleMeetings.setAccelerator(KeyStroke.getKeyStroke(
				KeyEvent.VK_M, InputEvent.ALT_MASK));
		mItemExitProgram.addActionListener(new ExitProgramHandler());
		mItemSaveFile.addActionListener(new SaveFileHandler());
		mItemCreateEmployee.addActionListener(new CreateEmployeeHandler());
		mItemRemoveEmployee.addActionListener(new RemoveEmployeeHandler());
		mItemRegisterSick.addActionListener(new RegisterSickHandler());
		mItemRegisterHealthy.addActionListener(new RegisterHealthyHandler());
		mItemRegisterLoggedIn.addActionListener(new RegisterLoggedInHandler());
		mItemRegisterLoggedOut
				.addActionListener(new RegisterLoggedOutHandler());
		mItemViewMeetings.addActionListener(new ViewMeetingsHandler());
		mItemScheduleMeetings.addActionListener(new ScheduleMeetingsHandler());
		return menuBar;
	}


	public void displayAllEmps()
	{
		allEmpModel.clear();
		String[] normalArray = new String[controller.getAllEmployees().size()];
		normalArray = controller.getAllEmployees().toArray(normalArray);

		for (int i = 0; i < normalArray.length; i++)
		{
			allEmpModel.addElement(normalArray[i]);
		}

		listOfAllEmps.setVisible(true);
		listOfSickEmps.setVisible(false);
		listOfLiEmps.setVisible(false);
		listOfLoEmps.setVisible(false);
	}

	public void displaySick()
	{
		sickEmpModel.clear();
		String[] normalArray = new String[controller.getSickEmployees().size()];
		normalArray = controller.getSickEmployees().toArray(normalArray);

		for (int i = 0; i < normalArray.length; i++)
		{
			sickEmpModel.addElement(normalArray[i]);
		}
		listOfAllEmps.setVisible(false);
		listOfSickEmps.setVisible(true);
		listOfLiEmps.setVisible(false);
		listOfLoEmps.setVisible(false);
	}

	public void displayLoggedIn()
	{
		loggedInEmpModel.clear();
		String[] normalArray = new String[controller.getLoggedInEmps().size()];
		normalArray = controller.getLoggedInEmps().toArray(normalArray);

		for (int i = 0; i < normalArray.length; i++)
		{
			loggedInEmpModel.addElement(normalArray[i]);
		}
		listOfAllEmps.setVisible(false);
		listOfSickEmps.setVisible(false);
		listOfLiEmps.setVisible(true);
		listOfLoEmps.setVisible(false);
	}

	public void displayLoggedOut()
	{
		loggedOutEmpModel.clear();
		String[] normalArray = null;
		normalArray = new String[controller.getLoggedOutEmps().size()];
		normalArray = controller.getLoggedOutEmps().toArray(normalArray);

		for (int i = 0; i < normalArray.length; i++)
		{
			loggedOutEmpModel.addElement(normalArray[i]);
		}
		listOfAllEmps.setVisible(false);
		listOfSickEmps.setVisible(false);
		listOfLiEmps.setVisible(false);
		listOfLoEmps.setVisible(true);
	}
	
	public void openXML()
	{
		controller.openFile();
		try
		{
			Thread.sleep(1000);
		}
		catch (InterruptedException e1)
		{
			e1.printStackTrace();
		}
		displayAllEmps();
		setOpenedState();
	}

	
	public void setOpenedState()
	{
		mItemSaveFile.setEnabled(true);
		mItemExitProgram.setEnabled(true);
		mItemCreateEmployee.setEnabled(true);
		mItemRemoveEmployee.setEnabled(true);
		mItemRegisterSick.setEnabled(true);
		mItemRegisterHealthy.setEnabled(false);
		mItemRegisterLoggedIn.setEnabled(true);
		mItemRegisterLoggedOut.setEnabled(true);
		mItemViewMeetings.setEnabled(false);
		mItemScheduleMeetings.setEnabled(true);
		displayComboBox.setEnabled(true);
	}
	
	public void setViewMeetingsClickable()
	{
		mItemViewMeetings.setEnabled(true);
	}
	
	public void showAllState()
	{
		mItemSaveFile.setEnabled(true);
		mItemExitProgram.setEnabled(true);
		mItemCreateEmployee.setEnabled(true);
		mItemRemoveEmployee.setEnabled(true);
		mItemRegisterSick.setEnabled(true);
		mItemRegisterHealthy.setEnabled(false);
		mItemRegisterLoggedIn.setEnabled(true);
		mItemRegisterLoggedOut.setEnabled(true);
		mItemScheduleMeetings.setEnabled(true);
		displayComboBox.setEnabled(true);
	}
	
	public void showSickState()
	{
		mItemSaveFile.setEnabled(true);
		mItemExitProgram.setEnabled(true);
		mItemCreateEmployee.setEnabled(false);
		mItemRemoveEmployee.setEnabled(false);
		mItemRegisterSick.setEnabled(false);
		mItemRegisterHealthy.setEnabled(true);
		mItemRegisterLoggedIn.setEnabled(false);
		mItemRegisterLoggedOut.setEnabled(false);
		mItemScheduleMeetings.setEnabled(true);
	}
	
	public void showLoggedInState()
	{
		mItemSaveFile.setEnabled(true);
		mItemExitProgram.setEnabled(true);
		mItemCreateEmployee.setEnabled(false);
		mItemRemoveEmployee.setEnabled(false);
		mItemRegisterSick.setEnabled(true);
		mItemRegisterHealthy.setEnabled(false);
		mItemRegisterLoggedIn.setEnabled(false);
		mItemRegisterLoggedOut.setEnabled(true);
		mItemScheduleMeetings.setEnabled(true);
	}
	
	public void showLoggedOutState()
	{
		mItemSaveFile.setEnabled(true);
		mItemExitProgram.setEnabled(true);
		mItemCreateEmployee.setEnabled(false);
		mItemRemoveEmployee.setEnabled(false);
		mItemRegisterSick.setEnabled(true);
		mItemRegisterHealthy.setEnabled(false);
		mItemRegisterLoggedIn.setEnabled(true);
		mItemRegisterLoggedOut.setEnabled(false);
	}
	

	public void showError(String text)
	{
		JOptionPane.showMessageDialog(new JFrame(), text, "Dialog",
				JOptionPane.ERROR_MESSAGE);
	}

	/**
	 * v HANDLERS v
	 */

	private class ScheduleMeetingsHandler implements ActionListener
	{
		public void actionPerformed(ActionEvent e)
		{

			SetMeetingsWindow smw = new SetMeetingsWindow(controller);
			smw.setResizable(false);
			setViewMeetingsClickable();
			if (!(smw.isVisible() == true))
			{
				smw.setVisible(true);
				smw.displayAllEmps(controller.getAllEmployees());
			}
			else
			{
				smw.setFocusable(true);
			}
		}
	}

	private class ViewMeetingsHandler implements ActionListener
	{
		public void actionPerformed(ActionEvent e)
		{
			ViewMeetingsWindow vmw = new ViewMeetingsWindow(controller);
			vmw.setResizable(false);
			if (!(vmw.isVisible()))
			{
				vmw.setVisible(true);
			}
			else
			{
				vmw.dispose();
			}
		}
	}
	

	private class DisplayBoxHandler implements ActionListener
	{
		public void actionPerformed(ActionEvent e)
		{
			if (displayComboBox.getSelectedItem().equals("Show all employees"))
			{
				displayAllEmps();
				showAllState();
			}
			else
				if (displayComboBox.getSelectedItem().equals(
						"Show sick employees"))
				{
					displaySick();
					showSickState();
				}
				else
					if (displayComboBox.getSelectedItem().equals(
							"Show employees logged in"))
					{
						displayLoggedIn();
						showLoggedInState();
					}
					else
						if (displayComboBox.getSelectedItem().equals(
								"Show employees logged out"))
						{
							displayLoggedOut();
							showLoggedOutState();
						}
		}
	}

	private class CreateEmployeeHandler implements ActionListener
	{
		public void actionPerformed(ActionEvent e)
		{
			String userName = JOptionPane.showInputDialog(null,
					"User name for employee");
			String passWord = JOptionPane.showInputDialog(null,
					"Password for employee");
			controller.createEmp(userName, passWord);
			displayAllEmps();
		}
	}

	private class RemoveEmployeeHandler implements ActionListener
	{
		public void actionPerformed(ActionEvent e)
		{

			String userName = listOfAllEmps.getSelectedValue().toString();
			int response = JOptionPane.showConfirmDialog(null,
					"Do you want to remove " + userName + "?", "Confirm",
					JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
			if (response == JOptionPane.NO_OPTION)
			{
				System.out.println("No button clicked");
			}
			else
				if (response == JOptionPane.YES_OPTION)
				{
					System.out.println("Yes button clicked");
					controller.removeEmp(listOfAllEmps.getSelectedValue().toString());
				}
				else
					if (response == JOptionPane.CLOSED_OPTION)
					{
						System.out.println("JOptionPane closed");
					}
			displayAllEmps();
		}
	}

	private class RegisterSickHandler implements ActionListener
	{
		public void actionPerformed(ActionEvent e)
		{

			if(listOfAllEmps.isVisible())   // everybody
			{
				String userName = listOfAllEmps.getSelectedValue().toString();
				controller.registerSick(userName);
				sickEmpModel.addElement(userName);
			}
			else if(listOfLoEmps.isVisible()) // logged out
			{
				String userNameFromLoggedOut = listOfLoEmps.getSelectedValue().toString();				
				controller.registerSick(userNameFromLoggedOut);
				sickEmpModel.addElement(userNameFromLoggedOut);
				loggedOutEmpModel.removeElement(userNameFromLoggedOut);
			}
			else // logged in
			{
				String userNameFromLoggedIn = listOfLiEmps.getSelectedValue().toString();				
				controller.registerSick(userNameFromLoggedIn);
			    controller.signUserOut(userNameFromLoggedIn);
				sickEmpModel.addElement(userNameFromLoggedIn);
				loggedInEmpModel.removeElement(userNameFromLoggedIn);
			}
			
		}
	}

	private class RegisterHealthyHandler implements ActionListener
	{
		public void actionPerformed(ActionEvent e)
		{

				String userNameFromSickList = listOfSickEmps.getSelectedValue().toString();				
				controller.registerHealthy(userNameFromSickList);
				sickEmpModel.removeElement(userNameFromSickList);
		}
	}

	private class RegisterLoggedInHandler implements ActionListener
	{
		public void actionPerformed(ActionEvent e)
		{
			
			if(listOfAllEmps.isVisible())
			{
				String userNameFromAll = listOfAllEmps.getSelectedValue().toString();   
				controller.signUserIn(userNameFromAll);
			}
			else
			{
				String userNameFromLoggedOut = listOfLoEmps.getSelectedValue().toString();
				controller.signUserIn(userNameFromLoggedOut);
				Object removeFromList = listOfLoEmps.getSelectedValue();
				loggedOutEmpModel.removeElement(removeFromList);	
			}
		}
	}

	private class RegisterLoggedOutHandler implements ActionListener
	{
		public void actionPerformed(ActionEvent e)
		{
			if(listOfAllEmps.isVisible())
			{
				String userName = listOfAllEmps.getSelectedValue().toString();
				controller.signUserOut(userName);
				loggedOutEmpModel.addElement(userName);
			}
			else
			{
				String userName = listOfLiEmps.getSelectedValue().toString();
				controller.signUserOut(userName);
				loggedInEmpModel.removeElement(userName);
				loggedOutEmpModel.addElement(userName);
			}
		}
	}

	private class ExitProgramHandler implements ActionListener
	{
		public void actionPerformed(ActionEvent e)
		{
			controller.exitProgram();
		}
	}
	

	private class SaveFileHandler implements ActionListener
	{
		public void actionPerformed(ActionEvent e)
		{
			controller.saveFile();
		}
	}

	/**
	 * Launch the application.
	 */
	public static void main(String[] args)
	{
		try
		{
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		}
		catch (UnsupportedLookAndFeelException e)
		{
			System.out.println("Unsupported Look");
		}
		catch (ClassNotFoundException e)
		{
			System.out.println("Class not found");
		}
		catch (InstantiationException e)
		{
			System.out.println("InstantiationException");
		}
		catch (IllegalAccessException e)
		{
			System.out.println("Illegal Access");
		}
		AdminController aController = new AdminController();
		AdminView frame = new AdminView(aController);
		frame.setVisible(true);
	}
}
