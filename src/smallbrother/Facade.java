package smallbrother;

import org.w3c.dom.Document;
import smallbrother.account.Account;
import smallbrother.account.Admin;
import smallbrother.account.User;
import smallbrother.exceptions.AccountException;
import smallbrother.xml.XMLFactory;
import smallbrother.xml.XMLPersonnelParser;
import javax.swing.*;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Serializable;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @author dv6
 */
public class Facade implements Serializable
{
	/**
	 * @uml.property name="admin"
	 * @uml.associationEnd
	 */
	private Admin admin;
	private Map<String, User> users = new HashMap<String, User>();
	long meetingId = 0;
	final JFileChooser fc = new JFileChooser();

	public synchronized ArrayList<String> getAllEmployees()
	{
		ArrayList<String> a = new ArrayList<String>();
		for (User u : users.values())
			a.add(u.getName());
		return a;
	}

	public synchronized ArrayList<String> getSickEmployees()
	{
		ArrayList<String> a = new ArrayList<String>();
		for (User u : users.values())
			if (u.isSick())
				a.add(u.getName());
		return a;
	}

	public synchronized ArrayList<String> getLoggedInEmps()
	{
		ArrayList<String> a = new ArrayList<String>();
		for (User u : users.values())
			if (u.isInsideBuilding())
				a.add(u.getName());
		return a;
	}

	public synchronized ArrayList<String> getLoggedOutEmps()
	{
		ArrayList<String> a = new ArrayList<String>();
		for (User u : users.values())
			if (!u.isInsideBuilding())
				a.add(u.getName());
		return a;
	}

	public ArrayList<MeetingList> getAllMeetings()
	{
		long meet = 0;
		ArrayList<MeetingList> meetings = new ArrayList<MeetingList>();
		while (meet <= meetingId)
		{
			MeetingList ml = new MeetingList();
			for (User u : users.values())
			{
				if (u.getMeetings() != null)
					for (Meeting m : u.getMeetings())
					{
						if (m.getId() == meet)
						{
							ml.setMeeting(m);
							ml.addParticipant(u.getName());
							// meetings.add(ml);
						}

					}
				if (!meetings.contains(ml) && (ml.getMeeting() != null))
					meetings.add(ml);
			}
			meet++;
		}
		return meetings;
	}

	public synchronized void registerSick(String userName)
	{
		for (User u : users.values())
			if (u.getName().equals(userName))
				u.setSick(true);
	}

	 public String getAllParticipantsForMeeting(int meetingID) {
	        String participants = "";
	        for (User u : users.values())
	            for (Meeting m : u.getMeetings()) {

	                if (m.getId() == meetingID) participants+=u.getName();
	            }
	        return participants.toString();
	    }
	public synchronized void registerHealthy(String userName)
	{
		for (User u : users.values())
			if (u.getName().equals(userName))
				u.setSick(false);
	}

	public synchronized void createUser(String name, String password)
	{
		try
		{
			if (!users.containsKey(name))
				users.put(name, new User(name, password));
			else
				throw new AccountException(name);
		}
		catch (AccountException exception)
		{
			System.out.println(exception.userExists());
		}
	}

	public synchronized void removeUser(String name)
	{
		try
		{
			if (users.containsKey(name))
				users.remove(name);
			else
				throw new AccountException(name);
		}
		catch (AccountException e)
		{
			System.out.println(e.userDoesNotExist());
		}
	}

	public synchronized void scheduleMeeting(ArrayList<String> userName,
			int day, int month, int year, int hour_begin, int minute_begin,
			int hour_end, int minute_end)
	{
		for (String s : userName)
			this.scheduleMeeting(s, day, month, year, hour_begin, minute_begin,
					hour_end, minute_end);
		meetingId++;
	}

	public synchronized void scheduleMeeting(String userName, int day,
			int month, int year, int hour_begin, int minute_begin,
			int hour_end, int minute_end)
	{

		try
		{
			DateFormat formatter;
			String date_str = day + "-" + month + "-" + year + "-" + hour_begin
					+ "-" + minute_begin;
			Date begin, end;
			formatter = new SimpleDateFormat("dd-mm-yy-hh-mm");
			begin = (Date) formatter.parse(date_str);
			date_str = day + "-" + month + "-" + year + "-" + hour_end + "-"
					+ minute_end;
			end = (Date) formatter.parse(date_str);
			Meeting meeting = new Meeting(begin, end);
			meeting.setId(meetingId);
			try
			{
				if (users.containsKey(userName))
					users.get(userName).scheduleMeeting(meeting);
				else
					throw new AccountException(userName);
			}
			catch (AccountException a)
			{
				System.out.println(a.userDoesNotExist());
			}
		}
		catch (ParseException e)
		{
			e.printStackTrace();
		}
	}

	@Override
	public String toString()
	{
		final StringBuilder sb = new StringBuilder();
		sb.append("Facade\tUsers:\n");
		// sb.append("{admin=").append(admin.toString());
		for (User u : users.values())
		{
			sb.append("Name: ").append(u.getName());
			sb.append(", Password: ").append(u.getPassword()).append("\n");
			if (u.getMeetings() != null)
			{
				for (Meeting m : u.getMeetings())
				{
					sb.append("\tMeeting: ").append(m.getId());
					sb.append(", Begin: ").append(m.getBegin());
					sb.append(" ,End: ").append(m.getEnd()).append("\n");
				}

			}
		}
		sb.append("\n");
		return sb.toString();
	}

	public String toXML()
	{
		final StringBuilder sb = new StringBuilder();
		// sb.append("<?xml version=\"1.0\" encoding=\"ISO-8859-1\" ?>\n<accounts>\n");
		sb.append("<?xml version=\"1.0\" encoding=\"UTF-8\" ?>\n<Personnel>\n");
		for (Account a : users.values())
			sb.append(XMLFactory.toXML(a));
		sb.append("</Personnel>");
		return sb.toString();
	}

	public synchronized void logout(String userName)
	{
		if (users.containsKey(userName))
			users.get(userName).setInsideBuilding(false);
		this.toFile();
	}

	// for users to login themselves
	public synchronized boolean login(String userName, String password)
	{
		if (users.containsKey(userName))
		{
			if (users.get(userName).getPassword().equals(password))
				users.get(userName).setInsideBuilding(true);
			else
			{
				System.out.println("Invalid password!");
				return false;
			}
		}
		else
			createUser(userName, password);
		this.toFile();
		return true;
	}

	public synchronized void login(String userName)
	{
		if (users.containsKey(userName))
			users.get(userName).setInsideBuilding(true);

	}

	public String getUser(String userName)
	{
		try
		{
			if (users.containsKey(userName))
				return XMLFactory.toXML(users.get(userName));
			else
				throw new AccountException(userName);
		}
		catch (AccountException exception)
		{
			System.out.println(exception.userDoesNotExist());
		}
		return null;
	}

	// TODO get meetings for specific user
	public String getMeetingsForUser(String userName)
	{
		String temp = "";
		try
		{
			if (users.containsKey(userName))
			{
				for (Meeting m : users.get(userName).getMeetings())

					temp += "Meeting ID:" + m.getId() + "\n From: "
							+ m.getBegin() + "\n To: \n " + m.getEnd() + "\n \n";
							}
			else
				throw new AccountException(userName);
		}
		catch (AccountException exception)
		{
			System.out.println(exception.userDoesNotExist());
		}
		return temp;
	}

	public boolean isInsideBuilding(String userName)
	{
		try
		{
			if (users.containsKey(userName))
				return users.get(userName).isInsideBuilding();
			else
				throw new AccountException(userName);
		}
		catch (AccountException exception)
		{
			System.out.println(exception.userDoesNotExist());
		}
		return null != null;
	}

	public boolean isSick(String userName)
	{
		try
		{
			if (users.containsKey(userName))
				return users.get(userName).isSick();
			else
				throw new AccountException(userName);
		}
		catch (AccountException exception)
		{
			System.out.println(exception.userDoesNotExist());
		}
		return null != null;
	}

	public void toFile()
	{
		try
		{
			FileWriter out = new FileWriter(fc.getSelectedFile());
			System.out.println(fc.getName());
			String s = this.toXML();
			out.write(s);
			out.close();
		}
		catch (IOException ex)
		{
			ex.printStackTrace();
		}
	}

	public void getAllDataFromXMLFile()
	{
		fc.setFileFilter(new javax.swing.filechooser.FileFilter()
		// fc.setCurrentDirectory(new File("."));  TODO starts the chooser in the jar dir
		{
			public boolean accept(File f)
			{
				return f.isDirectory()
						|| f.getName().toLowerCase().endsWith(".xml");
			}

			public String getDescription()
			{
				return "XML Files";
			}
		});
		int returnVal = fc.showOpenDialog(null);
		if (returnVal != JFileChooser.APPROVE_OPTION)
			System.exit(0);
		users.putAll(XMLPersonnelParser.parseXmlFile(fc.getSelectedFile()));
		meetingId = checkLastMeetingId();
	}

	private long checkLastMeetingId()
	{
		for (User u : users.values())
			for (Meeting m : u.getMeetings())
				if (meetingId < m.getId())
					meetingId = m.getId();
		return ++meetingId;
	}
}