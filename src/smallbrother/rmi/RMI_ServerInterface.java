package smallbrother.rmi;

import java.rmi.Remote;
import java.rmi.RemoteException;


public interface RMI_ServerInterface extends Remote
{
    public String sendXML(String userName) throws RemoteException;
    public void login(RMI_ClientInterface clientInterface, String userName, String password) throws RemoteException;
    public void logout(String userName) throws RemoteException;
    public boolean isInsideBuilding(String userName) throws RemoteException;
    public boolean isSick(String userName) throws RemoteException;
    public void isHealthy(String userName) throws RemoteException;
    public String getMeetingsForUser(String userName) throws RemoteException;
	public String getAllParticipantsForMeeting(int meetingID) throws RemoteException;
}
